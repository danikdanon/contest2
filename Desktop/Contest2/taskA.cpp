#include <iostream>
#include <vector>

using namespace std;

int max_ASCII = 256;

vector<int> lcp (vector<int> p, string s){
    int k = 0; // lcp у предыдущей пары
    int n = int(s.size());
    vector<int> lcp(n);
    vector<int> pos(n);
    for (int i = 0; i < n; i++)
        pos[p[i]] = i;
    
    for (int i = 0; i < n; i++){
        if (k > 0)
            k--; //значение lcp уменьшилось не более, чем на 1
        if (pos[i] == n-1){
            lcp[n-1] = -1;
            k = 0;
        }
        else{
            int j = p[pos[i] + 1]; // рассматриваем следующий суффикс
            while(i + k < n && j + k < n && s[j+k] == s[i+k])
                k++; // начинаем проверять, зная, что lcp>=k
        }
        lcp[pos[i]] = k;
    }
    return lcp;
}

int main() {
    string s;
    cin >> s;
    s += "$";
    int n = int(s.size());
    vector<int> p(n),cnt(max_ASCII),c(n);
    for (auto ch : s)
        cnt[ch]++;
    for (int i=1; i<max_ASCII; ++i)
        cnt[i] += cnt[i-1];
    for (int i=0; i<n; ++i)
        p[--cnt[s[i]]] = i;
    c[p[0]] = 0;
    int classes = 1;
    for (int i=1; i<n; ++i) {
        if (s[p[i]] != s[p[i-1]])  ++classes;
        c[p[i]] = classes-1;
    }
    
    
    vector<int> pn(n), cn(n);
    for (int h=0; (1<<h)<n; ++h) {
        for (int i=0; i<n; ++i) {
            pn[i] = p[i] - (1<<h);
            if (pn[i] < 0)  pn[i] += n;
        }
        cnt.clear();
        cnt.resize(classes);
        for (int i=0; i<n; ++i)
            ++cnt[c[pn[i]]];
        for (int i=1; i<classes; ++i)
            cnt[i] += cnt[i-1];
        for (int i=n-1; i>=0; --i)
            p[--cnt[c[pn[i]]]] = pn[i];
        cn[p[0]] = 0;
        classes = 1;
        for (int i=1; i<n; ++i) {
            int mid1 = (p[i] + (1<<h)) % n,  mid2 = (p[i-1] + (1<<h)) % n;
            if (c[p[i]] != c[p[i-1]] || c[mid1] != c[mid2])
                ++classes;
            cn[p[i]] = classes-1;
        }
        c = cn;
    }
    
    auto Lcp = lcp(p,s);
    int ans = 0;
    for (int i = 1; i < n; i++)
        ans += n - 1 - p[i] - Lcp[i];
    cout << ans << endl;
    
    return 0;
}

