#include <iostream>
#include <vector>

using namespace std;

vector<int> z_func (string s){
    int n = int(s.length());
    vector<int> z(n);
    int r = 0,l = 0,i = 1; // l и r - самый правый отрезок совпадения
    while (i < n) {
        if (i <= r)
            z[i] = min(r-i+1, z[i-l]);//r-i+1 - макс знач
        while (i+z[i] < n && s[z[i]] == s[i+z[i]]) //тупо сравниваем там, где не исследовано
            z[i]++;
        if (i + z[i] - 1 > r){
            l = i;
            r = i+z[i]-1;
        }
        i++;
    }
    return z;
}


int main() {
    string s,p,t;
    cin >> p >> s;
    t = p + '#' + s;
    auto z = z_func(t);
    for (int i = p.size(); i < t.size(); i++)// помнить про #
        if (z[i] == p.size())
            cout << i - p.size() - 1 << " "; // # есть
    return 0;
}
