#include <iostream>
#include <vector>

using namespace std;

void PiToStr(const vector <int> &pi){
    if (pi.size() == 0){
        cout << "" << endl;
    }
    
    string curr = "a"; // собираем строку
    int i = 1;
    while (i < pi.size()){
        if (pi[i] == 0){
            vector<bool> deprecated(26, false);
            int j = pi[i - 1]; // смотрим на первый символ, который не должен совп
            while (j > 0) {
                deprecated[curr[j] - 'a'] = true;
                j = pi[j - 1];  // аналогичный сдвиг, как и при построении z-функции
            }
            
            int min = 1;
            while (deprecated[min]){
                min++;
            }
            curr += ('a' + min);
        } else {
            curr += curr[pi[i] - 1]; // просто добавляем нужный символ
        }
        i++;
    }
    
    cout << curr << endl;
}



int main() {
    vector<int> pi;
    int value;
    while (cin >> value) {
        pi.push_back(value);
    }
    
    PiToStr(pi);
    return 0;
}
